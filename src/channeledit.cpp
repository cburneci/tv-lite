#include "channeledit.h"
#include "channel.h"
#include "vlcoptdlg.h"
#include <wx/msgdlg.h>
#include "debug.h"

using namespace tvlite;

CChannelEdit::CChannelEdit(wxWindow* parent):CChannelEditBase(parent)
{
    int width, height;
    m_addressList->GetSize(&width, &height);
    m_addressList->AppendTextColumn(_("Addresses"), wxDATAVIEW_CELL_EDITABLE, width * 2/3);
    m_addressList->AppendTextColumn(_("Name"), wxDATAVIEW_CELL_EDITABLE);
    m_sdbSizer3OK->SetDefault();
    m_editindex = wxNOT_FOUND;
}

CChannelEdit::~CChannelEdit()
{
}

void CChannelEdit::FillGroups()
{
   wxArrayString groups;
   ((MainFrame*)wxGetApp().GetMainFrame())->GetSelectedGroupFromLocalLists(groups);
   for (size_t i = 0; i <  groups.GetCount(); i++)
   {
      m_GroupBox->Append(groups[i]);
   }
}

void CChannelEdit::SetData(CChannel *channel)
{
   *m_channelName << channel->GetName();
   m_GroupBox->SetValue(channel->GetGroup());
   FillGroups();
   for (size_t i = 0; i < channel->GetStreamURLs().GetCount(); i++)
   {
      wxVector<wxVariant> data;
      data.push_back(channel->GetStreamURLs()[i]);
      data.push_back(channel->GetStreamNames()[channel->GetStreamURLs()[i]]);
      m_addressList->AppendItem(data);
   }
   m_vlcoptions = channel->GetVLCOptions();
}

void CChannelEdit::GetData(CChannel *channel)
{
   wxArrayString s;
   wxStringToStringHashMap  r;
   for (int i = 0; i < m_addressList->GetItemCount(); i++)
   {
      wxString addressString = m_addressList->GetTextValue(i,0).Trim().Trim(false);
      if (addressString != "" && addressString != "Click here to input the address...")
      {
         s.Add(addressString);
         wxString nameString = m_addressList->GetTextValue(i,1).Trim().Trim(false);
         r[addressString] = nameString;
      }
   }
   channel->SetStreamURLs(s);
   channel->SetStreamNames(r);
   channel->SetGroup(m_GroupBox->GetValue().Trim().Trim(false));
   channel->SetName(m_channelName->GetValue());
   channel->SetVLCOptions(m_vlcoptions);
   s.Clear();
}

bool CChannelEdit::ValidateData()
{
   if (m_channelName->GetValue() == "")
   {
      wxMessageBox(_("Please input a valid name"), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
      return false;
   }
   int count = 0;
   bool valid = false;
   for (int i = 0; i < m_addressList->GetItemCount(); i++)
   {
      wxString addressString = m_addressList->GetTextValue(i,0).Trim().Trim(false);
      wxString nameString = m_addressList->GetTextValue(i,1).Trim().Trim(false);
      valid = false;
      if (addressString != "" && addressString != _("Click here to input the address..."))
      {
         valid = true;
      }

      if (valid)
      {
         count++;
      }
      else
      {
         break;
      }
   }
   if (!valid)
   {
      wxMessageBox(_("Empty address detected "), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
   }
   if (count == 0)
   {
      wxMessageBox(_("Please, enter at least an address"), _("Validation error"), wxOK|wxICON_EXCLAMATION, this);
   }
   return valid;
}

void CChannelEdit::OnOkClicked( wxCommandEvent& event )
{
   if (ValidateData())
   {
      EndModal(wxID_OK);
   }
}

void CChannelEdit::OnAddAddress( wxCommandEvent& event )
{
      wxVector<wxVariant> data;
      data.push_back(_("Click here to input the address..."));
      data.push_back("");
      m_addressList->AppendItem(data);
      m_addressList->SelectRow(m_addressList->GetItemCount() - 1);
}

void CChannelEdit::OnDeleteAddress( wxCommandEvent& event )
{
   if (m_addressList->GetSelectedRow() != wxNOT_FOUND)
   {
      if (wxMessageBox(_("Do you really wish to delete the selected address?"),
                           _("Question"),
                             wxYES_NO|wxICON_QUESTION, NULL) == wxYES)
     {
         m_addressList->DeleteItem(m_addressList->GetSelectedRow());
     }

  }
}

void CChannelEdit::OnUpClicked(wxCommandEvent& event)
{
   if (m_addressList->GetSelectedRow() != wxNOT_FOUND && m_addressList->GetSelectedRow() >= 1)
   {
      int index1 = m_addressList->GetSelectedRow();
      wxString url1 = m_addressList->GetTextValue(index1, 0);
      wxString name1 = m_addressList->GetTextValue(index1, 1);
      wxString url2 = m_addressList->GetTextValue(index1 - 1, 0);
      wxString name2 = m_addressList->GetTextValue(index1 - 1, 1);
      m_addressList->SetTextValue(url2, index1, 0);
      m_addressList->SetTextValue(url1, index1 - 1, 0);
      m_addressList->SetTextValue(name2, index1, 1);
      m_addressList->SetTextValue(name1, index1 - 1, 1);
      m_addressList->SelectRow(index1 - 1);
      m_addressList->UnselectRow(index1);
   }
}

void CChannelEdit::OnDownClicked(wxCommandEvent& event)
{
 if (m_addressList->GetSelectedRow() != wxNOT_FOUND && m_addressList->GetSelectedRow() <= m_addressList->GetItemCount() - 2)
 {
      int index1 = m_addressList->GetSelectedRow();
      wxString url1 = m_addressList->GetTextValue(index1, 0);
      wxString url2 = m_addressList->GetTextValue(index1 + 1, 0);
      wxString name1 = m_addressList->GetTextValue(index1, 1);
      wxString name2 = m_addressList->GetTextValue(index1 + 1, 1);
      m_addressList->SetTextValue(url2, index1, 0);
      m_addressList->SetTextValue(url1, index1 + 1, 0);
      m_addressList->SetTextValue(name2, index1, 1);
      m_addressList->SetTextValue(name1, index1 + 1, 1);
      m_addressList->SelectRow(index1 + 1);
      m_addressList->UnselectRow(index1);
   }
}

void CChannelEdit::OnVLCOptClicked(wxCommandEvent& event)
{
   if (m_addressList->GetSelectedRow() != wxNOT_FOUND)
   {

      CVLCOptDlg* vlcOptDlg = new CVLCOptDlg(this);
      int index = m_addressList->GetSelectedRow();
      wxString url = m_addressList->GetTextValue(index, 0);
      wxArrayString vlcopt;
      CStringToArrayStringHashMap::iterator it = m_vlcoptions.find(url);
      if (it != m_vlcoptions.end())
      {
         vlcopt = it->second;
      }
      vlcOptDlg->SetData(vlcopt);

      int rc = vlcOptDlg->ShowModal();
      if (rc == wxID_OK)
      {
         vlcOptDlg->GetData(vlcopt);
         m_vlcoptions[url] = vlcopt;
      }
      vlcOptDlg->Destroy();
      delete vlcOptDlg;

   }

}


void CChannelEdit::OnAddressEditStarted( wxDataViewEvent& event )
{
   if (m_addressList->GetSelectedRow() != wxNOT_FOUND)
   {
      int index = m_addressList->GetSelectedRow();
      m_oldurl = m_addressList->GetTextValue(index, 0);
      m_editindex = index;
      CStringToArrayStringHashMap::iterator it = m_vlcoptions.find(m_oldurl);
      if (it != m_vlcoptions.end())
      {
         m_tempvlcopt = it->second;
      }
      
   }
}


void CChannelEdit::OnAddressEditDone( wxDataViewEvent& event )
{
  
}

void CChannelEdit::OnItemChanged( wxDataViewEvent& event )
{
   if (m_editindex != wxNOT_FOUND)
   {
      bool duplicatefound = false;
      if (m_addressList->GetSelectedRow() == m_editindex)
      {
         int index = m_addressList->GetSelectedRow();
         wxString newurl = m_addressList->GetTextValue(index, 0);
         DBG_INFO("newurl = %s", (const char*)newurl.utf8_str()); 
         for (int i = 0; i < m_addressList->GetItemCount(); i++)
         {
            if (index != i && newurl == m_addressList->GetTextValue(i, 0))
            {
               duplicatefound = true;
               break;
            }
         }
         if (!duplicatefound)
         {
            m_vlcoptions.erase(m_oldurl);
            m_vlcoptions[newurl] = m_tempvlcopt;
         }
         else
         {
            //Do not set back the old value - can enter infinite loop
            //m_addressList->SetTextValue(m_oldurl, index, 0);
            wxMessageBox(_("Found another record with the same URL!"), _("Error"), wxOK|wxICON_EXCLAMATION, this);
         }
         m_editindex = wxNOT_FOUND;
      }
   }
}
