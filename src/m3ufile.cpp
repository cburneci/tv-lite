#include "m3ufile.h"
#include "debug.h"

using namespace tvlite;

CM3UFile::CM3UFile(	const wxString &filename,
                     const wxString &sep,
                     const wxMBConv &conv):wxFile(filename, wxFile::read), m_conv(conv), m_sep(sep)
{
   m_buffer = nullptr;
   m_isInitialized = false;
}


CM3UFile::~CM3UFile()
{
   if (m_buffer != nullptr)
   {
      delete m_buffer;
   }
}

bool CM3UFile::Init()
{
  if (m_isInitialized)
  {
     return true;
  }
  bool rc = true;
  if (IsOpened())
  {
     if (m_buffer == nullptr)
     {
        m_buffer = new char[M3U_BUFFER_SIZE + 10];
        if (m_buffer == nullptr)
        {
           rc = false;
        }
     }
  }
  else
  {
     rc = false;
  }
  if (rc)
  {
     m_isInitialized = true;
     memset(m_buffer, '\0', M3U_BUFFER_SIZE + 10);
  }
  return rc;
}

bool CM3UFile::ReadLine(wxString &line)
{
   bool rc = true;
   int eolpos = wxNOT_FOUND;
   int eolsize = 0;
   if (!m_isInitialized)
   {
      rc = Init();
   }
   if (rc)
   {
      // try to read a line from the string
      while (eolpos == wxNOT_FOUND)
      {
         eolsize = 2;
         eolpos= m_string.Find("\r\n");
         if (eolpos == wxNOT_FOUND)
         {
            eolsize = 1;
            eolpos = m_string.Find("\n");
         }
         if (eolpos == wxNOT_FOUND)
         {
            //try to read more chars from buffer
            wxString tempString;
            bool result = ReadFromBuffer(tempString);
            if (!result)
            {
               //could not read more from the buffer
               rc = false;
               break;
            }
            m_string.Append(tempString);
         }
      }
      if (eolpos != wxNOT_FOUND)
      {
         //Trim left the string
         line = m_string.Left(eolpos);
         //DBG_INFO("Line is %s", (const char*)line.utf8_str());
         m_string = m_string.Mid(eolpos + eolsize);
      }
   }
   return rc;
}

bool CM3UFile::ReadFromBuffer(wxString &s)
{
   bool rc = true;
   memset(m_buffer, '\0', M3U_BUFFER_SIZE + 10);
   ssize_t count = Read(m_buffer, M3U_BUFFER_SIZE);
   if (count == 0)
   {
      rc = false;
   }
   else
   {
     s = wxString((const char*)m_buffer, m_conv);
     if (s.Length() == 0)
     {
        ssize_t pos = count;
        do
        {
           count = Read(m_buffer + pos++, 1);
           s = wxString((const char*)m_buffer, m_conv);
        }
        while (s.Length() == 0 && count == 1 && pos < 10);
     }
   }
   return rc;
}
