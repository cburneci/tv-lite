#include <wx/txtstrm.h>
#include "sopthread.h"
#include "debug.h"
#include "sopprotocolhandler.h"



tvlite::CSopThread::CSopThread(wxEvtHandler *handler, CSopProtocol *sopProcess): 
                           wxThread(wxTHREAD_JOINABLE),
                           m_sophandler(handler),
                           m_sopprocess(sopProcess),
                           m_running(true)
{
   DBG_INFO("Thread create");
}

tvlite::CSopThread::~CSopThread()
{
}

void tvlite::CSopThread::SetRunning(bool run)
{
   m_access.Lock();
   m_running = run;
   m_access.Unlock();
} 

bool tvlite::CSopThread::GetRunning()
{
   bool state;
   m_access.Lock();
   state = m_running;
   m_access.Unlock();
   return state;
}

wxThread::ExitCode tvlite::CSopThread::Entry()
{
 
   bool bailout = false;
   DBG_INFO("Thread entered");
   if (!m_sopprocess->IsInputOpened())
   {
      DBG_ERROR("Input not opened. Leaving");
      bailout = true;
   }
   if (!bailout)
   {   
      wxTextInputStream tis(*m_sopprocess->GetInputStream());
      while (GetRunning())
      {
         if (m_sopprocess->IsInputAvailable())
         {
            wxString msg;
            wxMutexGuiEnter();
            msg << tis.ReadLine();
            wxMutexGuiLeave();
            if (m_sopprocess->GetInputStream()->Eof())
            {
               DBG_ERROR("Eof from pipe. Exiting")
               bailout = true;
               break;
            }   
            printf("%s\n", (const char*)msg.c_str());
            int start = msg.Find("nblockAvailable=");
            if (start != wxNOT_FOUND)
            {
              wxString substr = msg.Mid(start);
              long nBlocks;
              sscanf((const char*)substr.c_str(),"nblockAvailable=%lu", &nBlocks);
              if (nBlocks > 60 && !bailout)
              {
                 DBG_INFO("Should start playing now!");
                 bailout = true;
                 //send play event
                 wxCommandEvent playevent(vlcEVT_PLAY);
                 wxQueueEvent(m_sophandler, playevent.Clone());
              }
            }
         }
         wxThread::Yield();
      }
      
    }
    if (bailout)
    {
       wxCommandEvent exitevent(thrEVT_EXIT);
       DBG_INFO("Send exit event");
       wxQueueEvent(m_sophandler,exitevent.Clone());
       SetRunning(false);
    }
    DBG_INFO("Thread exit");
    return NULL;
}
