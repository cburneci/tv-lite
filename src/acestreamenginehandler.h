#ifndef CACESTREAMENGINEHANDLER_H
#define CACESTREAMENGINEHANDLER_H

#include "baseprotocolhandler.h"
#include "aceprotocol.h"

namespace tvlite
{

class CAcestreamEngineHandler : public CBaseProtocolHandler
{
private:
   CAcestreamEngineHandler(const CAcestreamEngineHandler& rhs);
   CAcestreamEngineHandler& operator=(const CAcestreamEngineHandler& rhs);
   CAcestreamEngineHandler();
   CAceProtocol *m_process;
   long m_protopid;
   bool m_IsInstalled;
   void OnExitAceProcess(wxProcessEvent &event);

public:
   CAcestreamEngineHandler(wxEvtHandler *parent);
   virtual ~CAcestreamEngineHandler();
   virtual void Start() ;
   virtual void Stop() ;
   CAceProtocol *GetProcess()
   {
      return m_process;
   };
   bool IsEngineInstalled()
   {
      return m_IsInstalled;
   };
};

}

#endif // CACESTREAMPROTOCOLHANDLER_H
