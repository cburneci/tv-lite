#ifndef CBUSYOBJECT_H
#define CBUSYOBJECT_H

namespace tvlite
{

class CBusyObject
{
   private:
   bool m_busy;
public:
   CBusyObject();
   ~CBusyObject();
   void SetBusy();
   void ClearBusy();
   bool IsBusy();
};

class CBusyGuard
{
private:
   CBusyGuard();
   CBusyGuard(const CBusyGuard& rhs);
   CBusyGuard& operator=(const CBusyGuard& rhs);
   CBusyObject* m_parentobj;
   bool m_isInitialized;
public:
   CBusyGuard(CBusyObject* parent);
   ~CBusyGuard();
   bool Allows();
};


}

#endif // CBUSYOBJECT_H
