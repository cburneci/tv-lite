#include "channellistctrl.h"

using namespace tvlite;

channelListCtrl::channelListCtrl(wxWindow *parent, wxWindowID id, const wxPoint &pos, const wxSize &size,
                 long style, const wxValidator &validator, const wxString &name ):
                 wxListCtrl(parent, id, pos, size, style, validator, name)
{
}

channelListCtrl::~channelListCtrl()
{
}


wxString channelListCtrl::OnGetItemText (long item, long column) const
{
   if(item < m_channelList->Count())
   {
      return (*m_channelList)[item]->GetName();

   }

   else
   {
      return wxEmptyString;
   }
}

void channelListCtrl::SetChannelList(TPChannelList *channelList)
{
   m_channelList = channelList;
}
