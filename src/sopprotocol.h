#ifndef CSOPPROTOCOL_H
#define CSOPPROTOCOL_H

#include "baseprotocol.h"
#include "sopthread.h"

namespace tvlite
{

class CSopThread;

class CSopProtocol: public CBaseProtocol
{
private:
   CSopProtocol(const CSopProtocol& rhs) = delete;
   CSopProtocol& operator=(const CSopProtocol& rhs) = delete;
   CSopProtocol() = delete;
public:
   CSopProtocol(wxEvtHandler *parent, wxString url, wxString cmd = "");
   virtual ~CSopProtocol();

   virtual int  StartProtocol(long &pid);

   virtual int LoadConfig();
   virtual int SaveConfig();

   wxString GetUrl();
   void SetURL(wxString url);

   wxString GetCmd();
   void SetCmd(wxString cmd);

   int GetProtoport();
   void SetProtoPort(wxInt32 port);

   int GetPlayerPort();
   void SetPlayerPort(wxInt32 port);


private:
   wxInt32 m_playerport;
   wxInt32 m_protoport;
   long m_protopid;
   int SearchSop(wxString &cmd);

};

}

#endif // CSOPPROTOCOL_H
